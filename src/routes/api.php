<?php

Route::group(['namespace' => 'Bytecity\Tylersoft\Http\Controllers'], function () {
    Route::post('tylersoft/callback', 'TylersoftController@callback')->name('callback');
});