<?php

namespace Bytecity\Tylersoft;

use Illuminate\Support\ServiceProvider;


class TylersoftServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes/api.php');
        $this->mergeConfigFrom(
            __DIR__ . '/config/tylersoft.php',
            'tylersoft'
        );

        $this->publishes([
            __DIR__ . '/config/tylersoft.php' => config_path('tylersoft.php'),
        ]);

    }

    public function register()
    {
        $this->app->alias(Tylersoft::class, 'Tylersoft');
    }
}
