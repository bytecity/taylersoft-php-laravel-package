<?php

namespace Bytecity\Tylersoft;

use Bytecity\Tylersoft\Traits\ConsumesExternalServices;

class Tylersoft 
{
    use ConsumesExternalServices;

    protected $baseUri;

    protected $paymentUri;

    protected $clientId;

    protected $clientSecret;

    public function __construct(){
        $this->baseUri      = config('tylersoft.base_uri');
        $this->paymentUri   = config('tylersoft.paymentUri');
        $this->serviceId    = config('tylersoft.serviceId');
        $this->clientId     = config('tylersoft.client_id');
        $this->clientSecret = config('tylersoft.client_secret');
    }

    /**
     * Request to the end point of TYLERSOFT_BASE_URI to get the token. the pay method is also used below.
     */
    public function request($data)
    {
        session()->forget('tylersoftToken');

        $response = $this->makeRequest(
            'POST',
            $this->baseUri
        );

        session(['tylersoftToken' => $response->token]);

        return $this->pay($data);
    }

    /**
     * Request to the end point of TYLERSOFT_PAYMENT_URI by sending proper data also and returning the response
     */
    public function pay($data)
    {
        $response = $this->makeRequest(
            'POST',
            $this->paymentUri,
            [],
            [
                "amount"        => $data["amount"],
                "accountno"     => $data["accountno"],
                "serviceid"     => 1007,
                "msisdn"        => $data["msisdn"],
                "currencycode"  => $data["currencycode"],
                "name"          => $data["name"],
                "transactionid" => $data["transactionid"],
                'callbackurl'   => "https://f412-192-140-149-204.ngrok.io/callback",
                "email"         => "mompati@tylersoft.net"
            ],
            [],
            // the body above will be converted to json by guzzlehttp as we want to send a json request as thats what paypal requires
            $isJsonRequest = true
        );

        return $response;
    }

   
    protected function resolveAuthorization(&$queryParams, &$headers, &$formParams)
    {   
        if(session()->has('tylersoftToken')){
            $headers['Authorization'] = $this->resolveBearerAccessToken();
        }else{
            $headers['Authorization'] = $this->resolveBasicAccessToken();
        }
        
    }

    protected function resolveBasicAccessToken()
    {
        $credentials = base64_encode("{$this->clientId}:{$this->clientSecret}");
        return "Basic {$credentials}";
    }

    public function resolveBearerAccessToken()
    {
        // returning the bearer access token
        return "Bearer " . session('tylersoftToken');
    }

    protected function decodeResponse($response)
    {
        return json_decode($response);
    }
}