<?php
namespace Bytecity\Tylersoft\Facade;

use Illuminate\Support\Facades\Facade;

/**
 * Class Mpesa
 * @package Safaricom\Mpesa\Facade
 */
class Tylersoft extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Tylersoft';
    }


}