<?php
return [
    'base_uri'      => env('TYLERSOFT_BASE_URI'),
    'paymentUri'    => env('TYLERSOFT_PAYMENT_URI'),
    'serviceId'    => env('TYLERSOFT_SERVICE_ID'),
    'client_id'     => env('TYLERSOFT_CLIENT_ID'),
    'client_secret' => env('TYLERSOFT_CLIENT_SECRET')
];
