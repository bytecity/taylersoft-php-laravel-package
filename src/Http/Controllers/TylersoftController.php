<?php

namespace Bytecity\Tylersoft\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;


class TylersoftController extends Controller
{
    public function callback(Request $request)
    {
        if($request["response code"] == 100){
            $request->request->add([
                'transactionStatus' => "Successful",
                'description'       => "Successful transaction"
            ]);

        }elseif ($request["response code"] == 110) {
            $request->request->add([
                'transactionStatus' => "Partially Successful",
                'description'       => "Partial amount was approved"
            ]);

        }else{
            $request->request->add([
                'transactionStatus' => "Failed",
                'description'       => "Failed transaction"
            ]);

        }

        return response()->json($request->all());
    }
}
